//
//  QuestionCollectionViewCell.m
//  lovepotion
//
//  Created by Bryan Pratte on 1/27/15.
//  Copyright (c) 2015 quarkworks. All rights reserved.
//

#import "QuestionCollectionViewCell.h"

@implementation QuestionCollectionViewCell

+ (UINib*)nib {
    return [UINib nibWithNibName:NSStringFromClass([self class]) bundle:nil];
}

@end
