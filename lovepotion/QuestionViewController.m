//
//  QuestionViewController.m
//  lovepotion
//
//  Created by Bryan Pratte on 1/27/15.
//  Copyright (c) 2015 quarkworks. All rights reserved.
//

#import "QuestionViewController.h"
#import "QuestionCollectionViewCell.h"

@interface QuestionViewController ()  <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UICollectionView *questionCollectionView;
@property (strong, nonatomic) NSString* cellReuseIdentifier;

@end

@implementation QuestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    self.cellReuseIdentifier = NSStringFromClass([QuestionCollectionViewCell class]);
    [self.questionCollectionView registerNib:[QuestionCollectionViewCell nib] forCellWithReuseIdentifier:self.cellReuseIdentifier];

    UIEdgeInsets inset = self.questionCollectionView.contentInset;
    inset.top = 0;
    self.questionCollectionView.contentInset = inset;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 10;
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {

    QuestionCollectionViewCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:self.cellReuseIdentifier forIndexPath:indexPath];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(collectionView.bounds) / 1.0f, CGRectGetWidth(collectionView.bounds)/1.0f);
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
}


@end
