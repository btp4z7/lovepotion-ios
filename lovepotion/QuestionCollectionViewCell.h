//
//  QuestionCollectionViewCell.h
//  lovepotion
//
//  Created by Bryan Pratte on 1/27/15.
//  Copyright (c) 2015 quarkworks. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QuestionCollectionViewCell : UICollectionViewCell

+ (UINib*)nib;

@end
