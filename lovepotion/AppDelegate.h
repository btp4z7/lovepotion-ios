//
//  AppDelegate.h
//  lovepotion
//
//  Created by Bryan Pratte on 1/17/15.
//  Copyright (c) 2015 quarkworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Parse/Parse.h>
#import <ParseFacebookUtils/PFFacebookUtils.h>
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

